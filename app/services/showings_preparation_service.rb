class ShowingsPreparationService
	def self.call
		showings = Showing.all.to_a

		ordering =  %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday ).each_with_index.to_h
		showings = showings.sort_by.with_index { |showing, index| [ordering[showing[:day]], index] }

		showings.map do |showing| { room: Room.find(showing.room_id), day: showing.day,
		                            time: showing.time, film: Film.find(showing.film_id)
															}
		end
	end
end

