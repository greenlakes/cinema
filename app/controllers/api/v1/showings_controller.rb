module Api
	module V1
		class ShowingsController < ApplicationController
			def index
				showings = ::ShowingsPreparationService.call

				render json: {
						status: 'success',
						showings: showings.as_json(except: [:created_at, :updated_at])
				}
			end
		end
	end
end
