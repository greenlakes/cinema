class Showing < ApplicationRecord
  belongs_to :room
  belongs_to :film

  validates :day, :time, presence: true
end
