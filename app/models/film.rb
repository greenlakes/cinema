class Film < ApplicationRecord
	has_many :showings

	validates :name, :price, presence: true
end
