class Room < ApplicationRecord
	has_many :showings

	validates :name, :max_audience, presence: true
	validates :max_audience, numericality: true
end
