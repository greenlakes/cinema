require 'rails_helper'

RSpec.describe Showing, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:room) }
    it { is_expected.to belong_to(:film) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:day) }
    it { is_expected.to validate_presence_of(:time) }
  end
end