require 'rails_helper'

RSpec.describe Film, type: :model do
  describe 'associations' do
	  it { is_expected.to have_many(:showings) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:name) }
  end
end

