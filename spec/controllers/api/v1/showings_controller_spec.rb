require 'rails_helper'

RSpec.describe Api::V1::ShowingsController, type: :controller do

	# initialize test data
	before(:all) do
		Rails.application.load_seed
	end

	describe 'GET /index' do
		# make HTTP get request before each example
		before { get 'index' }

		it 'returns status code 200 and correct format' do
			expect(response).to have_http_status(200)
			expect(response.header['Content-Type']).to include 'application/json'
		end


		it 'returns correct film data' do
			json = JSON.parse(response.body)
			films_arr = json['showings'].map{|x| x['film']['name']}
			counts = Hash.new(0)
			films_arr.each { |name| counts[name] += 1 }

			expect(json).not_to be_empty
			expect(json.keys).to match_array(['status', 'showings'])
			expect(json['showings'].class).to be(Array)
			expect(json['showings'].size).to eq(42)

			# checking that each film is shown at least 3 times a week
			expect(counts.values.all?{ |n| n > 2 }).to be true

			expect(json['showings'][0]['room']['name']).to eq('Room1')
			expect(json['showings'][0]['day']).to eq('Sunday')
			expect(json['showings'][0]['time']).to eq('6PM')
			expect(json['showings'][1]['room']['name']).to eq('Room1')
			expect(json['showings'][1]['day']).to eq('Sunday')
			expect(json['showings'][1]['time']).to eq('8PM')
			expect(json['showings'][2]['room']['name']).to eq('Room2')
			expect(json['showings'][2]['day']).to eq('Sunday')
			expect(json['showings'][2]['time']).to eq('6PM')
			expect(json['showings'][3]['room']['name']).to eq('Room2')
			expect(json['showings'][3]['day']).to eq('Sunday')
			expect(json['showings'][3]['time']).to eq('8PM')
			expect(json['showings'][-2]['room']['name']).to eq('Room3')
			expect(json['showings'][-2]['day']).to eq('Saturday')
			expect(json['showings'][-2]['time']).to eq('6PM')
			expect(json['showings'][-1]['room']['name']).to eq('Room3')
			expect(json['showings'][-1]['day']).to eq('Saturday')
			expect(json['showings'][-1]['time']).to eq('8PM')
		end
	end
end
