class CreateShowings < ActiveRecord::Migration[5.1]
  def change
    create_table :showings do |t|
      t.references :room, foreign_key: true
      t.references :film, foreign_key: true
      t.string :time
      t.string :day

      t.timestamps
    end
  end
end
