30.times do
	Film.create({
			            name: Faker::Book.title,
			            price: rand(5..20)
	            })
end

3.times do |index|
	Room.create({
			            name: "Room#{ (index+1).to_s }",
			            max_audience: (index+1) * 100
	            })
end


times = ['6PM','8PM']
days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
rooms = []

Room.all.each { |room| rooms << room.id }

slots = times.product(days)
showings = rooms.product(slots)
showings= showings.map{|showing| showing.flatten}


films =  Film.all.to_a.map(&:id)

# showings.each_with_index { |showing, index| showing << films[index] }

showings = showings.map { |room_id, time, day| { room_id: room_id, time: time, day: day } }

showings.each do |showing|
	films.shift if Showing.where(film_id: films.first).count > 2
	Showing.create({
			               room_id: showing[:room_id],
			               time: showing[:time],
			               day: showing[:day],
			               film_id: films.first
	               })
end

