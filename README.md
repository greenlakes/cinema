# README

API Endpoints

Our API will expose the following RESTful endpoints:

GET /api/v1/showings/index	List all films on offer for a week (in both cinema rooms)